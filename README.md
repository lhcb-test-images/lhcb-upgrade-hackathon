Image with lhcb-upgrade-hackathon nightly 
=================================================

This docker image can be used to run the lhcb-upgrade test.

Building
--------

```
make
```

Pushing to the registry
-----------------------

```
make push
```


Running the test
----------------

```
docker run --privileged --rm -it gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon
```

To check the container:


```
docker run --privileged --rm -it gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon bash
```


