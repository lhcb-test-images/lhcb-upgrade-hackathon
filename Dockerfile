FROM gitlab-registry.cern.ch/lhcb-test-images/lhcb-data

ARG platform1=x86_64-centos7-gcc62-opt
ARG platform2=x86_64+avx2+fma-centos7-gcc62-opt
ARG SLOT=lhcb-upgrade-hackathon
ARG BUILDID=56
ARG TESTDIR=/opt/test
ARG LCGVER=92

# Now installing the nightlies
RUN mkdir -p /opt/nightlies && \
   source /opt/lhcb/LbLoginDev.sh && \
   cd  /opt/nightlies && \
   echo lbn-install --platforms=$platform1  $SLOT $BUILDID && \
   lbn-install --platforms=$platform1  $SLOT $BUILDID && \
   echo lbn-install --platforms=$platform2  $SLOT $BUILDID && \
   lbn-install --platforms=$platform2  $SLOT $BUILDID

# Adding the repo with the tests
RUN mkdir -p $TESTDIR && \
  cd $TESTDIR && \
  curl -o  MINBIASTESTSAMPLE_big_GEC_small.mdf  https://cernbox.cern.ch/index.php/s/Bx03mzVq49yr0tY/download 

# Adding numactl which is needed for the test
RUN yum -y install numactl

# Now adding the test directory
RUN cd $TESTDIR && git clone https://gitlab.cern.ch/lhcb/lhcb-benchmark-scripts.git && \
    cd $TESTDIR/lhcb-benchmark-scripts && git checkout c122b6e02ed1c4853d506f931ceda286e3fc250f 

# Create the setenv scripts
RUN source /opt/lhcb/LbLoginDev.sh && \
    for p in $platform1 $platform2; do \
	CMTPROJECTPATH=/opt/nightlies/$SLOT/$BUILDID:${CMTPROJECTPATH} lb-run -c $p --force-platform -i --sh Brunel/TDR > $TESTDIR/setenv.$p.sh; \
	echo "export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:/opt/lhcb/lcg/releases/LCG_${LCGVER}/gcc/6.2.0/x86_64-centos7/lib" >> $TESTDIR/setenv.$p.sh; \
	echo  "export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:/opt/lhcb/lcg/releases/LCG_${LCGVER}/gcc/6.2.0/x86_64-centos7/lib64" >> $TESTDIR/setenv.$p.sh; \
    done

    
RUN for p in $platform1 $platform2; do \
    echo "export CMTCONFIG=$p" > $TESTDIR/runtest.$p && \
	echo "source $TESTDIR/setenv.$p.sh" >> $TESTDIR/runtest.$p && \
	echo "export PATH=$TESTDIR/lhcb-benchmark-scripts:\${PATH}"  >> $TESTDIR/runtest.$p && \
	echo "export PYTHONPATH=$TESTDIR/lhcb-benchmark-scripts:\${PYTHONPATH}"  >> $TESTDIR/runtest.$p && \
	echo "export GITCONDDBPATH=/opt/git-conddb"  >> $TESTDIR/runtest.$p && \
	echo "export CURTESTDIR=$TESTDIR/workspace/$p" >> $TESTDIR/runtest.$p && \
	echo "mkdir -p \$CURTESTDIR && cd  \$CURTESTDIR && measureThroughput.sh /opt/test/MINBIASTESTSAMPLE_big_GEC_small.mdf"  >> $TESTDIR/runtest.$p && \
	chmod +x $TESTDIR/runtest.$p;  \
     done

# FIXME: Special extras for avx2+fma...
RUN curl http://lhcb-rpm.web.cern.ch/lhcb-rpm/misc/extra.x86_64+avx2+fma-centos7-gcc62-opt >> $TESTDIR/setenv.x86_64+avx2+fma-centos7-gcc62-opt.sh

ENV TESTDIR $TESTDIR
ENV DEFAULT_PLATFORM $platform2
CMD $TESTDIR/runtest.$DEFAULT_PLATFORM
